import React from 'react';
import { StyleSheet, View } from 'react-native';

const Footer = () =>{
    return (
        <View style={styles.footer}>

        </View>
    );
}
const styles=StyleSheet.create({
    footer:{
        width:'100%',
        height:75,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    button:{
        padding:20
    }

});

export default Footer;
